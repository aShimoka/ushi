//
//  IslandAsyncTasks.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the island async tasks
#include "Island/IslandAsyncTasks.h"

// Include the PointConverter class.
#include "Tools/Map/PointConverter.h"
// Include the log category.
#include "Tools/Log.h"

// Include the math functions.
#include <cmath>

// Implement the noise generator's setup method.
void UNoiseMapGeneratorTask::Setup(
    float* map,
    FIslandParameters* parameters,
    size_t chunkId,
    UPerlinGenerator* generator,
    float slice,
    FGenericPlatformProcess::FSemaphore semaphore
) {
    // Assign all the variables.
    _mpMap = map; _mpParams = parameters; _mChunkId = chunkId;
    _mpGenerator = generator; _mSlice = slice; _mSemaphore = semaphore;
}

// Implement the RunTask.
void UNoiseMapGeneratorTask::RunTask_Implementation() {
    // Get the starting point from the chunk's index.
    UPointConverter::Point2D chunkStart = UPointConverter::GetChunkPoint<UPointConverter::Point2D>(
        _mChunkId,
        _mpParams->LatticeResolution,
        _mpParams->LatticeResolution / _mpParams->ChunkCount
    );
    
    // Loop through the chunk.
    const size_t CHUNK_WIDTH = (_mpParams->LatticeResolution / _mpParams->ChunkCount);
    for (size_t y = chunkStart.y; y < chunkStart.y + CHUNK_WIDTH; y++)
    for (size_t x = chunkStart.x; x < chunkStart.x + CHUNK_WIDTH; x++) {
        // Get the position of the point on the map.
        float u = (float)x / _mpParams->LatticeResolution; float v = (float)y / _mpParams->LatticeResolution;
        
        // Get the noise for the point.
        float noise(0.f); float totalWeight(0.f);
        for (size_t level = 0; level < _mpParams->NoiseLevels; level++) {
            // Compute the noise.
            float weight = pow(_mpParams->NoiseWeight, level);
            noise += _mpGenerator->GetNoiseAt(
                u * pow(2, level),
                v * pow(2, level),
                _mSlice
            ) * weight;
            totalWeight += weight;
        }
        
        // Remove the total weight from the noise.
        noise = noise / totalWeight;
        
        // Compute the altered location.
        size_t alteredLocation = UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x, y }, _mpParams->LatticeResolution);
        
        // Lock the semaphore.
        _mSemaphore.Lock();
        
        // Store the variable.
        _mpMap[alteredLocation] = noise;
        
        // Unlock the semaphore.
        _mSemaphore.Unlock();
    }
}
