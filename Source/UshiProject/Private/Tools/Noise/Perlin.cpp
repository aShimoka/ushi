//
//  Perlin.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the perlin header.
#include "Tools/Noise/Perlin.h"
// Include the log declaration.
#include "Tools/Log.h"

// Include the standard random library.
#include <random>

// Permutation buffer size.
const unsigned int PERMUTATION_BUFFER_SIZE = 256;

// Create the default constructor.
UPerlinGenerator::UPerlinGenerator() :
    _mpPermutationBuffer(nullptr) {}

// Define the seeded constructor.
UPerlinGenerator* UPerlinGenerator::CreateGenerator(int seed, UObject* owner) {
    // Create a new generator instance.
    UPerlinGenerator* generator = NewObject<UPerlinGenerator>(owner);
    
    // Get a new random engine.
    std::default_random_engine engine(seed);
    
    // Generate the buffer.
    unsigned char* pBuf = new unsigned char[PERMUTATION_BUFFER_SIZE * 2];
    
    // Fill the buffer with all numbers from 0 to PERMUTATION_BUFFER_SIZE - 1.
    for (int i = 0; i < PERMUTATION_BUFFER_SIZE; i++) pBuf[i] = i;
    
    // Shuffle the contents of the buffer.
    for (int i = PERMUTATION_BUFFER_SIZE; i > 0; i--) {
        // Get a random index from the list.
        int j = engine() % (i + 1);
        
        // Swap both values.
        int tmp = pBuf[i];
        pBuf[i] = pBuf[j];
        pBuf[j] = tmp;
    }
    
    // Copy the shuffled values into the second part of the buffer.
    for (int i = 0; i < PERMUTATION_BUFFER_SIZE; i++) pBuf[i + PERMUTATION_BUFFER_SIZE] = pBuf[i];
    
    // Create the shared pointer responsible for the buffer.
    generator->_mpPermutationBuffer = TSharedPtr<const unsigned char>(pBuf);
    
    // Return the instance.
    return generator;
}

const float UPerlinGenerator::GetNoiseAt(float x, float y, float z) const {
    // If the permutation buffer is invalid, return 0.
    if (!_mpPermutationBuffer.IsValid()) {
        UE_LOG(UshiLog, Warning, TEXT("Queried an invalid perlin generator instance."))
        return 0;
    }
    
    // Get the permutation array.
    const unsigned char * p = _mpPermutationBuffer.Get();
    
    // Find the unit cube that contains the point
    int X = (int) floor(x) & 255;
    int Y = (int) floor(y) & 255;
    int Z = (int) floor(z) & 255;

    // Find relative x, y,z of point in cube
    x -= floor(x);
    y -= floor(y);
    z -= floor(z);

    // Compute fade curves for each of x, y, z
    double u = _Fade(x);
    double v = _Fade(y);
    double w = _Fade(z);

    // Hash coordinates of the 8 cube corners
    int A  = p[X] + Y;
    int AA = p[A] + Z;
    int AB = p[A + 1] + Z;
    int B  = p[X + 1] + Y;
    int BA = p[B] + Z;
    int BB = p[B + 1] + Z;

    // Add blended results from 8 corners of cube
    double res = _Lerp(
        w,
        _Lerp(
            v,
            _Lerp(
                u,
                _Grad(p[AA], x, y, z),
                _Grad(p[BA], x-1, y, z)
            ),
            _Lerp(
                u,
                _Grad(p[AB], x, y-1, z),
                _Grad(p[BB], x-1, y-1, z)
            )
        ),
        _Lerp(
            v,
            _Lerp(
                u,
                _Grad(p[AA+1], x, y, z-1),
                _Grad(p[BA+1], x-1, y, z-1)
            ),
            _Lerp(
                u,
                _Grad(p[AB+1], x, y-1, z-1),
                _Grad(p[BB+1], x-1, y-1, z-1)
            )
        )
    );
    return (res + 1.0)/2.0;
}


inline const float UPerlinGenerator::_Fade(float t) const {
    return t * t * t * (t * (t * 6 - 15) + 10);
}

inline const float UPerlinGenerator::_Lerp(float t, float a, float b) const {
    return a + t * (b - a);
}

inline const float UPerlinGenerator::_Grad(int hash, float x, float y, float z) const {
    int h = hash & 15;
    // Convert lower 4 bits of hash into 12 gradient directions
    double u = h < 8 ? x : y,
           v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

