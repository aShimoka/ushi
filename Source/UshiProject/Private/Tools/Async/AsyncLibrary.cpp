//
//  AsyncLibrary.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the AsyncLibrary header.
#include "Tools/Async/AsyncLibrary.h"

// Include the log category.
#include "Tools/Log.h"

// Create the runtask default implementation.
void UAsyncTask::RunTask_Implementation() {
    // Log a warning message.
    UE_LOG(UshiLog, Warning, TEXT("Called the RunTask method on the base UAsyncTask class. Have you correctly overriden the method ?"));
}

// Create a new instance of the asynchronous worker.
FAsyncWorker::FAsyncWorker(UAsyncTask* task) {
    // Create the unique pointer to the task.
    _mpTask = task;
}

// Execute the task.
void FAsyncWorker::DoWork() {
    // Check if the pointer is valid.
    if (_mpTask) {
        // Run the task.
        _mpTask->RunTask();
    } else {
        // Log an error.
        UE_LOG(UshiLog, Error, TEXT("A FAsyncWorker instance was executed without a task !"));
    }
}

TSharedPtr<UAsyncLibrary::Worker> UAsyncLibrary::RunTaskAsync(UAsyncTask* task) {
    // Create a new FAsyncWorker instance.
    TSharedPtr<UAsyncLibrary::Worker> worker = TSharedPtr<UAsyncLibrary::Worker>(new UAsyncLibrary::Worker(task));
    
    // Run the worker.
    worker->StartBackgroundTask();
    
    // Return the worker.
    return worker;
}
