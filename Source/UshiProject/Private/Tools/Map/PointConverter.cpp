//
//  PointConverter.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the PointConverted header.
#include "Tools/Map/PointConverter.h"

// Define the GetPointIndex for the 2D point.
template<>
UPointConverter::PointIndex UPointConverter::GetPointIndex<UPointConverter::Point2D>(Point2D point, size_t width) {
    // Compute the point's index.
    return point.x + point.y * width;
}

// Define the GetPointIndex for the 3D point.
template<>
UPointConverter::PointIndex UPointConverter::GetPointIndex<UPointConverter::Point3D>(Point3D point, size_t width) {
    // Compute the point's index.
    return point.x + point.y * width + point.z * width * width;
}

// Define the GetPoint for the 2D point.
template<>
UPointConverter::Point2D UPointConverter::GetPoint<UPointConverter::Point2D>(PointIndex point, size_t width) {
    // Compute the point from the index.
    return {
        (point) % width,
        point / width,
    };
}

// Define the GetPoint for the 3D point.
template<>
UPointConverter::Point3D UPointConverter::GetPoint<UPointConverter::Point3D>(PointIndex point, size_t width) {
    // Compute the point from the index.
    return {
        (point) % width,
        (point / width) % width,
        (point / width) / width,
    };
}

// Define the GetPoint for the 2D point.
template<>
UPointConverter::Point2D UPointConverter::GetChunkPoint<UPointConverter::Point2D>(ChunkIndex chunk, size_t width, size_t chunkCount) {
    // Compute the point from the index.
    return UPointConverter::GetPoint<UPointConverter::Point2D>(
        ((chunk % chunkCount) * (width / chunkCount)) +
        ((chunk / chunkCount) * ((width + 1) * (width / chunkCount))),
        width
    );
}
