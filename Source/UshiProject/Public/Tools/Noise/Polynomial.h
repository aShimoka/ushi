//
//  Polynomial.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the blueprint library class.
#include "Kismet/BlueprintFunctionLibrary.h"

// Include the generated header.
#include "Polynomial.generated.h"

/**
 * Polynoimial noise generator.
 * Template static class used to manipulate polynomial functions.
 * Can generate parameters from specified characteristics.
 *
 * @author Caillaud Jean-Baptiste.
 */
UCLASS()
class UPolynomialNoise : public UBlueprintFunctionLibrary {
    GENERATED_BODY()
    
    // ---  SubObjects ---
        // -- Public Types --
public:
            /**
             * Parameters used in a constant polynomial.
             * Equation form: f(x) = ax^0
             */
            struct Constant { float a; };
            
            /**
             * Parameters used in a linear polynomial.
             * Equation form: f(x) = ax^0 + bx^1
             */
			struct Linear { float a, b; };
            
            /**
             * Parameters used in a quadratic polynomial.
             * Equation form: f(x) = ax^0 + bx^1 + cx^2
             */
            struct Quadratic { float a, b, c; };
            
            /**
             * Parameters used in a cubic polynomial.
             * Equation form: f(x) = ax^0 + bx^1 + cx^2 + dx^3
             */
            struct Cubic { float a, b, c, d; };
    // --- /SubObjects ---
    
    // ---  Methods ---
        // -- Public Methods --
public:
            /**
             * Computes the result of the specified polynomial.
             *
             * @tparam T The type of the polynomial to compute. (One of Constant, Linear, Cubic or Quadratic).
             * @param factors The parameters passed to the method.
             * @param x The value of the computed variable.
             * @returns The result of the polynomial equation.
             */
            template<typename T>
            static double ComputePolynomial(T& factors, double x);
            
            /**
             * Generates a displacement polynomial.
             * Uses a height,width, origin and extent angles.
             *
             * @param height The height of the displacement at the origin,
             * @param width The total width of the displacement function.
             * @param originAngle The angle of the displacement function at the origin.
             * @param extentAngle The angle of the displacement function at the extent.
             * @param factors The output factors of the computation.
             * @see https://www.desmos.com/calculator/ypzgyfoioi for more info.
             */
            static void GenerateDisplacementFactors(double height, double width, double originAngle, double extentAngle, Cubic& factors);
    // --- /Methods ---
};
