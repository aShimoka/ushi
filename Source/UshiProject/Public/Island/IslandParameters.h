//
//  IslandParameters.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the engine minimal library.
#include "CoreMinimal.h"

// Include the generated header.
#include "IslandParameters.generated.h"

/**
 * Structure object that defines the island creation parameters.
 *
 * @author Caillaud Jean-Baptiste.
 */
USTRUCT(BlueprintType)
struct FIslandParameters {
    GENERATED_BODY()
        
        // ---  Attributes ---
            // -- Public Attributes --
    public:
                // - Lattice Parameters -
                    /**
                     * Defines the resolution used for the lattice.
                     * Aka. how many vertices are used on each axis of the lattice.
                     */
                    UPROPERTY(Category="Lattice", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", ClampMax = "1024"))
                    int LatticeResolution;
                    
                    /**
                     * Defines the number of chunks in the lattice.
                     * i.e. The number of sections in the procedural mesh.
                     * This is used to speed up alterations in the lattice.
                     */
                    UPROPERTY(Category="Lattice", EditAnywhere, BlueprintReadWrite, meta = (ClampMin="1", ClampMax="1024"))
                    int ChunkCount;
                    
                // - Noise Parameters -
                    /**
                     * Defines the noise used by the random generator for the island.
                     */
                    UPROPERTY(Category="Noise", EditAnywhere, BlueprintReadWrite)
                    int Seed;
                    
                    /**
                     * Defines the number of noise passes that are used during the generation of the island.
                     * Aka. the level of details applied to the noise texture.
                     */
                    UPROPERTY(Category="Noise", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "1", ClampMax = "16", UIMin = "1", UIMax = "16"))
                    int NoiseLevels;
                    
                    /**
                     * Defines the weight of each one of the noise levels.
                     * Aka. the amount of displacement caused by fine detail noise passes.
                     */
                    UPROPERTY(Category="Noise", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "1", UIMin = "0", UIMax = "1"))
                    float NoiseWeight;
                    
                    /**
                     * Defines the factor applied to the overall noise map.
                     */
                    UPROPERTY(Category="Noise", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "8", UIMin = "0", UIMax = "8"))
                    float NoiseFactor;
                    
                // - Island Plateau Parameters -
                    /**
                     * Defines the base height of the island's plateau, before any noise generation.
                     */
                    UPROPERTY(Category="Island|Plateau", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "1", UIMin = "0", UIMax = "1"))
                    float PlateauBaseHeight;
                    
                    /**
                     * Defines the base width of the island's plateau.
                     */
                    UPROPERTY(Category="Island|Plateau", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "1", UIMin = "0", UIMax = "1"))
                    float PlateauWidth;
                
                // - Island Coast Parameters -
                    /**
                     * Defines the width of the island's coast.
                     */
                    UPROPERTY(Category="Island|Coast", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "1", UIMin = "0", UIMax = "1"))
                    float CoastWidth;
                    
                    /**
                     * Defines the angle (in degrees) of the coast's origin.
                     */
                    UPROPERTY(Category="Island|Coast", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "-90", ClampMax = "90", UIMin = "-90", UIMax = "90"))
                    float CoastOriginAngle;
                    
                    /**
                     * Defines the angle (in degrees) of the coast's extent.
                     */
                    UPROPERTY(Category="Island|Coast", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "-90", ClampMax = "90", UIMin = "-90", UIMax = "90"))
                    float CoastExtentAngle;

                // - Island Bottom Parameters -
                    /**
                     * Defines the base height of the island's bottom, before any noise generation.
                     */
                    UPROPERTY(Category="Island|Bottom", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "0", ClampMax = "1", UIMin = "0", UIMax = "1"))
                    float BottomHeight;
                    
                    /**
                     * Defines the angle (in degrees) of the bottom's origin.
                     */
                    UPROPERTY(Category="Island|Bottom", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "-90", ClampMax = "90", UIMin = "-90", UIMax = "90"))
                    float BottomOriginAngle;
                    
                    /**
                     * Defines the angle (in degrees) of the bottom's extent.
                     */
                    UPROPERTY(Category="Island|Bottom", EditAnywhere, BlueprintReadWrite, meta = (ClampMin = "-90", ClampMax = "90", UIMin = "-90", UIMax = "90"))
                    float BottomExtentAngle;
        // --- /Attributes ---
};
